=begin
名前やメールアドレス等、シンプルなデータはハッシュで管理できる。
しかし、データの種類が増え、構造が複雑になると、ハッシュだけでは管理が難しくなる。
そこで、クラスを利用して管理していく。
=end

class User
  attr_writer :name, :email, :password, :gender #指定した変数の書き換えを許可する
  attr_reader :name, :email, :password, :gender #指定した変数の読み出しを許可する



  def initialize(name, email, password, gender = "不明") #初期化メソッド
    @name = name
    @email = email
    @password = password
    @gender = gender
  end

###クラスメソッド
  def self.male(name, email, password)
    new(name, email, password, "男")
  end

  def self.female(name, email, password)
    new(name, email, password, "女")
  end

###インスタンスメソッド
  def show
    puts <<-EOS
         -----------------------------------------
         ユーザー名：#{name}
         性別：#{gender}
         メールアドレス：#{email}
         パスワード：#{password}
         -----------------------------------------
         EOS
  end

end

users = []
users << User.new("たける", "permil.sample@gmail.com", "hogehoge", "男")

users << User.female("みつこ", "permil.sample2@gmail.com", "fugafuga")
users << User.male("やまだ", "permil.sample3@gmail.com", "hogefuga")

users.each do |user|
  user.show
end

__END__

    ◯クラス名はキャメルケースで！

    ◯インスタンスメソッド……インスタンスに含まれるデータを読み書きするメソッド
    ◯クラスメソッド……インスタンスに含まれるデータを使用しないメソッド



　
