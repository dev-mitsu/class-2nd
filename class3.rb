class Product
  attr_accessor :name, :price


  def initialize(name, price) #初期化メソッド
    @name = name
    @price = price
  end

  def show #インスタンスメソッド
    puts "----------------------"
    puts "商品名 : #{name}"
    puts "値段 : ¥#{price}"
  end
end

class PC < Product
  attr_accessor :os

  def initialize(name, price, os)
    super(name, price)

    @os = os
  end

  def show
    super
    puts "OS : #{os}"
  end
end

class DVD < Product
  attr_accessor :time

  def initialize(name, price, time)
    super(name, price)
    @time = time
  end

  def show
    super
    puts "再生時間 : #{time}分"
  end
end

class Food < Product
  attr_accessor :calory

  def initialize(name, price, calory)
    super(name, price)
    @calory = calory
  end

  def show
    super
    puts "カロリー : #{calory}kcal"
  end
end

food1 = Food.new("ハンバーガー", 200,300)

class Sweets < Food
  attr_accessor :suger

  def initialize(name, price, calory, suger)
    super(name, price, calory)
    @suger = suger
  end

  def show
    super
    puts "糖質 : #{suger}グラム"
  end
end




pc1 = PC.new("パソコンA", 150000, "Mac")
pc2 = PC.new("パソコンB", 100000, "Windows")
pc1.show
pc2.show

dvd1 = DVD.new("アイアンマン", 5000, 125)
dvd1.show

hambarger = Food.new("ハンバーガー", 200, 300)
hambarger.show

shortcake = Sweets.new("ショートケーキ", 100, 300, 30)
shortcake.show
