#　①ハッシュの基本( {キー => 値} )

user = { "name" => "Takemitsu", "email" => "permil.sample@gmail.com" }

puts user["name"]



#　②ハッシュのキーにシンボルを利用する。
user = { :name => "Takemitsu", :email => "permil.sample@gmail.com" }

puts user[:name]

#　Railsではシンボルが大量に使われているので、シンボルの理解は超重要。


#　②のコードは、次のように書いても同様の評価になる(この書き方が最も多い)。

user = { name: "Takemitsu", email: "permil.sample@gmail.com" }

puts "名前:#{user[:name]} メールアドレス:#{user[:email]}"


##ハッシュ関連のメソッド

puts user.keys #キーを一覧表示
puts user.values #値を一覧表示
