#①

class Vehicle
  attr_accessor :name, :cc, :tires
  def initialize(name, cc, tires)
    @name = name
    @cc = cc
    @tires = tires
  end

  def show
    puts "名前：#{name}, 排気量：#{cc}cc, タイヤの数：#{tires}本"
  end

  def reset_name(new_name)
    @name = new_name
  end
end

vehicle1 = Vehicle.new("自転車", 0, 2)
vehicle1.show
vehicle1.reset_name("トラック")
vehicle1.show





#②
class Vehicle
  attr_accessor :name, :cc, :tires #読みも書きも許可！
  def initialize(name, cc, tires)
    @name = name
    @cc = cc
    @tires = tires
  end

  def show()
    puts "名前：#{name}, 排気量：#{cc}cc, タイヤの数：#{tires}本"
  end

  def reset_name(name)
    @name = name
  end
end

vehicle3 = Vehicle.new("一輪車", 0, 1)
vehicle3.show
vehicle3.reset_name("リヤカー")
vehicle3.show

#③
class Car < Vehicle
  attr_accessor :plate_number #読みも書きも許可！
  def initialize(name, cc, tires, plate_number)
    super(name,cc,tires)
    @plate_number = plate_number
  end

  def show()
    puts "名前：#{name}, 排気量：#{cc}cc, タイヤの数：#{tires}本, ナンバー：#{plate_number}"
  end

  def reset_name(name)
    super
  end
end
