# ブロックを使用するメソッドの一部

5.times do |i| #5回処理を繰り返すメソッド(ただし、iは0から始まる)
  puts "[LOG] #{i+1}回目の実行です。"
end

__END__

numbers = [1, 2, 3, 4, 5]
new_numbers =  numbers.map do |number| #numbersに入った変数を3乗して新しい配列を作り直すメソッド
             number*number*number
           end
puts new_numbers
